#! /usr/bin/env python

import sys
import argparse
import tarfile
import numpy as np
from collections import OrderedDict


class Energy:

    def __init__(self, arr, dim=None):
        self.dim = dim if dim else len(arr)
        self.data = np.array(arr).reshape(-1, self.dim)

    def __add__(self, other):
        return Energy(np.concatenate((self.data, other.data), 0), dim=self.dim)

    def __repr__(self):
        return str(self.data)

    def part(self, i, j):
        return (
            Energy(self.data[np.ix_(i, i)], len(i)),
            Energy(self.data[np.ix_(j, j)], len(j)),
            Energy(self.data[np.ix_(arr_compl(i, self.dim), arr_compl(j, self.dim))], len(i))
        )

    def diag(self):
        return np.trace(self.data)

    def sum(self):
        return np.sum(self.data)

    def total(self, d=None, s=None):
        return 0.5 * ((s if s else self.sum()) + (d if d else self.diag()))

    def off_diag(self, t=None, d=None):
        d = d if d else self.diag()
        return (t if t else self.total(d=d)) - d


def arr_compl(arr, max_index):
    return [index for index in range(max_index) if index not in arr]


def read_inp(arch):
    for index, line in enumerate(arch.extractfile(arch.getmember('INP'))):
        if index == 1:
            return [int(w) for w in line.split()]


def read_seq(arch):
    chains = OrderedDict()
    for line in arch.extractfile(arch.getmember('SEQ')):
        ch = line[12]
        if ch not in chains:
            chains[ch] = 0
        chains[ch] += 1
    return chains.keys()


def read_traf(arch):
    traf_ = {}
    for line in arch.extractfile(arch.getmember('TRAF')):
        if '.' in line:
            words = line.split()
            energy = Energy([float(w) for w in words[2:-2]])
            key_ = (int(words[-1]), int(words[0]))
            val = traf_.get(key_)
            if val is not None:
                energy = val + energy
            traf_[key_] = energy
    return traf_


def read_cbs(filename):
    with tarfile.open(filename, 'r:gz') as arch:
        return read_inp(arch), read_seq(arch), read_traf(arch)


def receptor_indices(all_chains, rec_chains):
    return [i for i, ch in enumerate(all_chains) if ch in rec_chains]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='getEnergy',
        description='Program reads CABSdock trajectory file *.cbs and extracts energy components. '
                    'Output columns are (1)replica (2)step (3)Ereceptor (4)Etotal (5)Einteraction. '
                    'For detailed energy components use -m to output square energy matrices, where '
                    'A[i, j] is energy of interaction of the i-th and j-th chain.'
    )

    parser.add_argument(
        'input',
        metavar='FILE',
        help='name of the .cbs file'
    )

    parser.add_argument(
        'receptor',
        metavar='RECEPTOR',
        help='single word with all receptor chains i.e. AB'
    )

    parser.add_argument(
        '-o', '--output',
        metavar='OUTPUT',
        help='write output to OUTFILE, stdout otherwise'
    )

    parser.add_argument(
        '-m', '--out-matrices',
        metavar='OUTFILE',
        help='save complete energy matrices to OUTFILE.'
    )

    args = parser.parse_args()
    inp, seq, traf = read_cbs(args.input)
    rec_ind = receptor_indices(seq, args.receptor)
    pep_ind = arr_compl(rec_ind, len(seq))

    output = OrderedDict()
    mat_file = open(args.out_matrices, 'wb') if args.out_matrices else None

    np.set_printoptions(suppress=True, precision=3)

    for key, matrix in sorted(traf.items()):
        if mat_file:
            mat_file.write('%s\n%s\n\n' % (str(key), str(matrix)))

        rec_matrix, pep_matrix, bind_matrix = matrix.part(rec_ind, pep_ind)
        output[key] = (matrix.total(), rec_matrix.total(), pep_matrix.sum())

    if mat_file:
        mat_file.close()

    f = open(args.output, 'wb') if args.output else sys.stdout
    for k, v in output.items():
        f.write('%i %i %10.3f %10.3f %10.3f\n' % (k + v))
